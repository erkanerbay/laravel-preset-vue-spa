<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="MobileOptimized" content="on">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#ffffff">
	<meta name="description" content="description">
	<title>Title</title>
	<link rel="stylesheet" href="/css/app.css">
</head>

<body class="w-100 min-vh-100 d-flex flex-column justify-content-between">
	@include ('layouts.header')
	<div id="app">
		<main class="container">
			<router-view></router-view>
		</main>
	</div>
	@include ('layouts.footer')
	<script src="/js/app.js"></script>
</body>

</html>