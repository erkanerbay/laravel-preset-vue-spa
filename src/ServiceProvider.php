<?php

namespace EE\LaravelPreset;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Illuminate\Foundation\Console\PresetCommand;

class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        PresetCommand::macro(
            'EE',
            function ($command) {
                Preset::install();
                $command->info('Run "npm install && npm run start" to compile your fresh scaffolding.');
            }
        );
    }
}
