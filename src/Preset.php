<?php

namespace EE\LaravelPreset;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Console\Presets\Preset as LaravelPreset;

class Preset extends LaravelPreset
{
  public static function install()
  {
    static::updatePackages();
    static::updateMix();
    static::updateScripts();
    static::updateStyles();
  }

  public static function updatePackageArray($packages)
  {
    return array_merge(
      ['laravel-mix-tailwind' => '^0.1.0'],
      Arr::except(
        $packages,
        [
          'popper.js',
          'lodash',
          'jquery',
          'bootstrap'
        ]
      )
    );
  }

  public static function updateMix()
  {
    copy(__DIR__ . '/stubs/js/webpack.mix.js', base_path('webpack.mix.js'));
  }

  public static function updateScripts()
  {
    copy(__DIR__ . '/stubs/js/app.js', resource_path('assets/js/app.js'));
    copy(__DIR__ . '/stubs/js/bootstrap.js', resource_path('assets/js/bootstrap.js'));
    copy(__DIR__ . '/stubs/js/routes.js', resource_path('assets/js/routes.js'));
    copy(__DIR__ . '/stubs/js/components', resource_path('assets/js/components'));
  }

  public static function updateStyles()
  {
    File::cleanDirectory(resource_path('assets/sass'));
    copy(__DIR__ . '/stubs/styles', resource_path('assets/sass'));
  }
}
